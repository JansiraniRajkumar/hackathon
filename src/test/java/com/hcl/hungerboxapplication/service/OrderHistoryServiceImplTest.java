package com.hcl.hungerboxapplication.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.hungerboxapplication.dao.EmployeeRepository;
import com.hcl.hungerboxapplication.dao.OrderItemRepository;
import com.hcl.hungerboxapplication.model.Employee;
import com.hcl.hungerboxapplication.model.Item;
import com.hcl.hungerboxapplication.model.Order;
import com.hcl.hungerboxapplication.model.OrderItems;
import com.hcl.hungerboxapplication.model.Vendor;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OrderHistoryServiceImplTest {
	
	@InjectMocks
	OrderHistoryServiceImpl historyServiceImpl;
	
	@Mock
	EmployeeRepository employeeRepository;

	@Mock
	OrderItemRepository orderItemRepository;

	@Test
	public void TestOrderListByEmployeeForPositive() {
		
		Vendor vendor=new Vendor();
		vendor.setStallNumber(7670);
		vendor.setVendorId(1L);
		vendor.setVendorName("dosatalks");
		
		Item item=new Item();
		item.setItemDescription("suprrr");
		item.setItemId(1L);
		item.setItemType("veg");
		item.setName("doasa");
		item.setUnitPrice(30.0);
		item.setVendor(vendor);
	
		Employee emp=new Employee();
		emp.setEmail("dhaya@gmail.com");
		emp.setEmployeeId(1L);
		emp.setEmployeeName("dhayananthan");
		emp.setPassword("778249day");
		emp.setPhone(887044827);
		
		Order order=new Order();
		order.setEmployee(emp);
		order.setOrderId(1L);
		order.setOrderPrice(80.0);
		order.setQuantity(3);
			
		List<OrderItems> list=new ArrayList<>();
		OrderItems orderItems=new OrderItems();
		orderItems.setId(1L);
		orderItems.setItem(item);
		orderItems.setOrder(order);
		list.add(orderItems);
		
		Mockito.when(employeeRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(emp));
		Mockito.when(orderItemRepository.findByEmployee(Mockito.any())).thenReturn(list);
		
		List<OrderItems> result=historyServiceImpl.orderListByEmployee(emp.getEmployeeId());
		assertNotNull(result);
		
	}
	
	@Test
	public void TestOrderListByEmployeeForNegative() {
		
		Vendor vendor=new Vendor();
		vendor.setStallNumber(7670);
		vendor.setVendorId(-1L);
		vendor.setVendorName("dosatalks");
		
		Item item=new Item();
		item.setItemDescription("suprrr");
		item.setItemId(-1L);
		item.setItemType("veg"); 
		item.setName("doasa");
		item.setUnitPrice(30.0);
		item.setVendor(vendor);
	
		Employee emp=new Employee();
		emp.setEmail("dhaya@gmail.com");
		emp.setEmployeeId(1L);
		emp.setEmployeeName("dhayananthan");
		emp.setPassword("778249day");
		emp.setPhone(887044827);
		
		Order order=new Order();
		order.setEmployee(emp);
		order.setOrderId(1L);
		order.setOrderPrice(80.0);
		order.setQuantity(3);
			
		List<OrderItems> list=new ArrayList<>();
		OrderItems orderItems=new OrderItems();
		orderItems.setId(1L);
		orderItems.setItem(item);
		orderItems.setOrder(order);
		list.add(orderItems);
		
		Mockito.when(employeeRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(emp));
		Mockito.when(orderItemRepository.findByEmployee(Mockito.any())).thenReturn(list);
		
		List<OrderItems> result=historyServiceImpl.orderListByEmployee(emp.getEmployeeId());
		assertNotNull(result);
		
	}
	
}
