package com.hcl.hungerboxapplication.service;

import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;
import com.hcl.hungerboxapplication.dao.EmployeeRepository;
import com.hcl.hungerboxapplication.dao.ItemRepository;
import com.hcl.hungerboxapplication.dao.OrderItemRepository;
import com.hcl.hungerboxapplication.dao.OrderRepository;
import com.hcl.hungerboxapplication.dao.VendorRepository;
import com.hcl.hungerboxapplication.dto.OrderDto;
import com.hcl.hungerboxapplication.exception.EmployeeNotFoundException;
import com.hcl.hungerboxapplication.exception.PaymentNotSuccessfulException;
import com.hcl.hungerboxapplication.exception.VendorNotFoundException;
import com.hcl.hungerboxapplication.model.Employee;
import com.hcl.hungerboxapplication.model.Item;
import com.hcl.hungerboxapplication.model.Order;
import com.hcl.hungerboxapplication.model.OrderItems;
import com.hcl.hungerboxapplication.model.Vendor;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OrderServiceImplTest {

	@InjectMocks
	OrderServiceImpl orderServiceImpl;

	@Mock
	OrderItemRepository orderItemRepository;

	@Mock
	EmployeeRepository employeeRepository;

	@Mock
	VendorRepository vendorRepository;

	@Mock
	OrderRepository orderRepository;

	@Mock
	ItemRepository itemRepository;

	@Mock
	RestTemplate restTemplate;

	@Test
	public void TestMakeOrderForPositive()
			throws JSONException, EmployeeNotFoundException, VendorNotFoundException, PaymentNotSuccessfulException {

		Vendor vendor = new Vendor();
		vendor.setStallNumber(7670);
		vendor.setVendorId(1L);
		vendor.setVendorName("dosatalks");

		List<Item> list = new ArrayList<>();
		Item item = new Item();
		item.setItemDescription("suprrr");
		item.setItemId(1L);
		item.setItemType("veg");
		item.setName("doasa");
		item.setUnitPrice(30.0);
		item.setVendor(vendor);
		list.add(item);

		OrderDto orderDtos = new OrderDto();
		orderDtos.setEmployeeId(1L);
		orderDtos.setVendorId(1L);
		orderDtos.setItems(list);

		Employee emp = new Employee();
		emp.setEmail("dhaya@gmail.com");
		emp.setEmployeeId(1L);
		emp.setEmployeeName("dhayananthan");
		emp.setPassword("778249day");
		emp.setPhone(887044827);

		Order order = new Order();
		order.setEmployee(emp);
		order.setOrderId(1L);
		order.setOrderPrice(80.0);
		order.setQuantity(3);

		OrderItems orderItems = new OrderItems();
		orderItems.setId(1L);
		orderItems.setItem(item);
		orderItems.setOrder(order);

		Mockito.when(employeeRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(emp));
		Mockito.when(vendorRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(vendor));
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(order);
		Mockito.when(itemRepository.findByItemIdAndVendor(Mockito.anyLong(), Mockito.any())).thenReturn(item);
		Mockito.when(orderItemRepository.save(Mockito.any())).thenReturn(orderItems);

		Order result = orderServiceImpl.makeOrder(orderDtos);
		assertNotNull(result);

	}

	@Test
	public void TestMakeOrderForNegative()
			throws JSONException, EmployeeNotFoundException, VendorNotFoundException, PaymentNotSuccessfulException {

		Vendor vendor = new Vendor();
		vendor.setStallNumber(7670);
		vendor.setVendorId(-1L);
		vendor.setVendorName("dosatalks");

		List<Item> list = new ArrayList<>();
		Item item = new Item();
		item.setItemDescription("suprrr");
		item.setItemId(-1L);
		item.setItemType("veg");
		item.setName("doasa");
		item.setUnitPrice(30.0);
		item.setVendor(vendor);
		list.add(item);

		OrderDto orderDtos = new OrderDto();
		orderDtos.setEmployeeId(1L);
		orderDtos.setVendorId(1L);
		orderDtos.setItems(list);

		Employee emp = new Employee();
		emp.setEmail("dhaya@gmail.com");
		emp.setEmployeeId(1L);
		emp.setEmployeeName("dhayananthan");
		emp.setPassword("778249day");
		emp.setPhone(887044827);

		Order order = new Order();
		order.setEmployee(emp);
		order.setOrderId(1L);
		order.setOrderPrice(80.0);
		order.setQuantity(3);

		OrderItems orderItems = new OrderItems();
		orderItems.setId(1L);
		orderItems.setItem(item);
		orderItems.setOrder(order);

		Mockito.when(employeeRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(emp));
		Mockito.when(vendorRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(vendor));
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(order);
		Mockito.when(itemRepository.findByItemIdAndVendor(Mockito.anyLong(), Mockito.any())).thenReturn(item);
		Mockito.when(orderItemRepository.save(Mockito.any())).thenReturn(orderItems);

		Order result = orderServiceImpl.makeOrder(orderDtos);
		assertNotNull(result);

	}

}
