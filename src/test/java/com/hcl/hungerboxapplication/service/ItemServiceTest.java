package com.hcl.hungerboxapplication.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.hungerboxapplication.dao.ItemRepository;
import com.hcl.hungerboxapplication.exception.ItemNotFoundException;
import com.hcl.hungerboxapplication.model.Item;
import com.hcl.hungerboxapplication.model.Vendor;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ItemServiceTest {
	
	@InjectMocks
	ItemService itemServiceImpl;
	
	@Mock
	ItemRepository itemRepository;
	
	@Test
	public void TestGetItemByItemNameStartingWithForPositive() throws ItemNotFoundException {
		
		Vendor vendor = new Vendor();
		vendor.setStallNumber(2379);
		vendor.setVendorId(-1L);
		vendor.setVendorName("Give me 5");

		List<Item> list = new ArrayList<>();
		Item items = new Item();
		items.setItemDescription("tasty");
		items.setItemId(1L);
		items.setItemType("veg");
		items.setName("juice");
		items.setUnitPrice(30.0);
		items.setVendor(vendor);
		list.add(items);
		
		Mockito.when(itemRepository.findItemByNameLike(Mockito.anyString())).thenReturn(list);
		 List<Item> result=itemServiceImpl.viewItemByName(items.getName());
		 assertNotNull(result);
		
	}
	
	@Test
	public void TestGetItemByItemNameStartingWithForNegative() throws ItemNotFoundException {
		
		Vendor vendor = new Vendor();
		vendor.setStallNumber(2379);
		vendor.setVendorId(-1L);
		vendor.setVendorName("Give me 5");

		List<Item> list = new ArrayList<>();
		Item items = new Item();
		items.setItemDescription("tasty");
		items.setItemId(-1L);
		items.setItemType("veg");
		items.setName("juice");
		items.setUnitPrice(30.0);
		items.setVendor(vendor);
		list.add(items);
		
		Mockito.when(itemRepository.findItemByNameLike(Mockito.anyString())).thenReturn(list);
		 List<Item> result=itemServiceImpl.viewItemByName(items.getName());
		 assertNotNull(result);
		
	}


}
