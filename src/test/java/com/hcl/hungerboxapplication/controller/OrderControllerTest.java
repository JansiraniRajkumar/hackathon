package com.hcl.hungerboxapplication.controller;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.hungerboxapplication.dto.OrderDto;
import com.hcl.hungerboxapplication.exception.EmployeeNotFoundException;
import com.hcl.hungerboxapplication.exception.PaymentNotSuccessfulException;
import com.hcl.hungerboxapplication.exception.VendorNotFoundException;
import com.hcl.hungerboxapplication.model.Employee;
import com.hcl.hungerboxapplication.model.Order;
import com.hcl.hungerboxapplication.model.Vendor;
import com.hcl.hungerboxapplication.service.OrderServiceImpl;



@RunWith(MockitoJUnitRunner.Silent.class)
public class OrderControllerTest {
	
	@InjectMocks
	OrderController orderController;
	
	@Mock
	OrderServiceImpl orderServiceImpl;
	static OrderDto orderDto=null;
	Employee employee=null;
	static Order order=null;
	static Vendor vendor=null;
	@Before
	public void setUp() {
	orderDto=new OrderDto();
	 employee=new Employee();
	Vendor vendor=new Vendor();
	order=new Order();
	employee.setEmail("bhavani@123");
	vendor.setVendorId(1L);
	employee.setEmployeeId(1L);
	
	order.setEmployee(employee);
	orderDto.setEmployeeId(1l);
	orderDto.setVendorId(1l);
	}
	
	@Test
	public void testMakingOrderForPositive() throws EmployeeNotFoundException, VendorNotFoundException, JSONException, PaymentNotSuccessfulException {
		order.setOrderId(1L);
		Mockito.when(orderServiceImpl.makeOrder(orderDto)).thenReturn(order);
		ResponseEntity<Order> o1=orderController.makingOrder(orderDto);
		Assert.assertEquals(HttpStatus.OK,o1.getStatusCode());
		
		
		
	}
	@Test
	public void testMakingOrder() throws EmployeeNotFoundException, VendorNotFoundException, JSONException, PaymentNotSuccessfulException {
		order.setOrderId(1L);
		employee.setEmployeeId(2L);
		Mockito.when(orderServiceImpl.makeOrder(orderDto)).thenReturn(order);
		ResponseEntity<Order> o1=orderController.makingOrder(orderDto);
		Assert.assertEquals(HttpStatus.OK,o1.getStatusCode());
		
		
		
	}
	@Test
	public void testMakeOrder() throws EmployeeNotFoundException, VendorNotFoundException, JSONException, PaymentNotSuccessfulException {
		
		Mockito.when(orderServiceImpl.makeOrder(orderDto)).thenReturn(order);
		ResponseEntity<Order> o1=orderController.makingOrder(orderDto);
		Assert.assertNotNull(o1);
		
		
		
	}
}
