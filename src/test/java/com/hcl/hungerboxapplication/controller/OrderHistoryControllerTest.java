package com.hcl.hungerboxapplication.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.hungerboxapplication.model.OrderItems;
import com.hcl.hungerboxapplication.service.OrderHistoryService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OrderHistoryControllerTest {
	
	
	@InjectMocks
	OrderHistoryController orderHistoryController;
	
	@Mock
	OrderHistoryService orderHistoryService; 
	
	@Test
	public void TestGetByEmployeeIdForPositive() {
		
		List<OrderItems> list=new ArrayList<>();
		OrderItems orderItems=new OrderItems();
		orderItems.setId(1L);
		
		
		Mockito.when(orderHistoryService.orderListByEmployee(Mockito.anyLong())).thenReturn(list);
		ResponseEntity<List<OrderItems>> result=orderHistoryController.getByEmployeeId(Mockito.anyLong());
		assertEquals( HttpStatus.OK,result.getStatusCode());
		
	}

	
	@Test
	public void TestGetByEmployeeIdForNegative() {
		
		List<OrderItems> list=new ArrayList<>();
		OrderItems orderItems=new OrderItems();
		orderItems.setId(-1L);
		
		
		Mockito.when(orderHistoryService.orderListByEmployee(Mockito.anyLong())).thenReturn(list);
		ResponseEntity<List<OrderItems>> result=orderHistoryController.getByEmployeeId(Mockito.anyLong());
		assertEquals( HttpStatus.OK,result.getStatusCode());
		
	}

	
}
