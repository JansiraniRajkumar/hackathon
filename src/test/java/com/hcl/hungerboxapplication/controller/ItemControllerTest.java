package com.hcl.hungerboxapplication.controller;


import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.hungerboxapplication.exception.ItemNotFoundException;
import com.hcl.hungerboxapplication.model.Item;
import com.hcl.hungerboxapplication.model.Vendor;
import com.hcl.hungerboxapplication.service.ItemService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ItemControllerTest {

	@InjectMocks
	ItemController itemController;

	@Mock
	ItemService itemService;

	@Test
	public void TestGetItemByItemNameLikeForPositive() throws ItemNotFoundException {

		Vendor vendor = new Vendor(); 
		vendor.setStallNumber(2379);
		vendor.setVendorId(1L);
		vendor.setVendorName("Give me 5");

		List<Item> list = new ArrayList<>();
		Item items = new Item();
		items.setItemDescription("tasty");
		items.setItemId(1L);
		items.setItemType("veg");
		items.setName("juice");
		items.setUnitPrice(30.0);
		items.setVendor(vendor);

		Mockito.when(itemService.viewItemByName(Mockito.anyString())).thenReturn(list);
		ResponseEntity<List<Item>> result = itemController.getItemByName(items.getName());
		assertNotNull(result);

	}
	
	@Test
	public void TestGetItemByItemNameLikeForNegative() throws ItemNotFoundException {

		Vendor vendor = new Vendor();
		vendor.setStallNumber(2379);
		vendor.setVendorId(-1L);
		vendor.setVendorName("Give me 5");

		List<Item> list = new ArrayList<>();
		Item items = new Item();
		items.setItemDescription("tasty"); 
		items.setItemId(1L);
		items.setItemType("veg");
		items.setName("juice");
		items.setUnitPrice(30.0);
		items.setVendor(vendor);

		Mockito.when(itemService.viewItemByName(Mockito.anyString())).thenReturn(list);
		ResponseEntity<List<Item>> result = itemController.getItemByName(items.getName());
		assertEquals(HttpStatus.OK,result.getStatusCode());

	}


}
