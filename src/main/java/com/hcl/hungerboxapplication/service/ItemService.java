package com.hcl.hungerboxapplication.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.hungerboxapplication.dao.ItemRepository;
import com.hcl.hungerboxapplication.exception.ItemNotFoundException;
import com.hcl.hungerboxapplication.model.Item;



/**
 * 
 * @author dhayananthan
 * this is class for item service
 *
 */

@Service
public class ItemService {
	
	Logger logger = LoggerFactory.getLogger(ItemService.class);
	
	@Autowired
	ItemRepository itemRepository;
	
	public List<Item> viewItemByName(String name) throws ItemNotFoundException {
		logger.info("From the Item Service");
		List<Item> items = itemRepository.findItemByNameLike("%" + name + "%");
			 if(items.isEmpty()) {
				 throw new ItemNotFoundException("item with given name is not found");
			 } 
			 return items;
		}



}
