package com.hcl.hungerboxapplication.service;

import java.util.List;

import com.hcl.hungerboxapplication.exception.EmployeeNotFoundException;
import com.hcl.hungerboxapplication.model.OrderItems;

public interface OrderHistoryService {

	List<OrderItems> orderListByEmployee(long employeeId) throws EmployeeNotFoundException;

}
