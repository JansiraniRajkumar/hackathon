package com.hcl.hungerboxapplication.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hcl.hungerboxapplication.dao.EmployeeRepository;
import com.hcl.hungerboxapplication.dao.OrderItemRepository;
import com.hcl.hungerboxapplication.exception.EmployeeNotFoundException;
import com.hcl.hungerboxapplication.model.Employee;
import com.hcl.hungerboxapplication.model.OrderItems;

@Service
public class OrderHistoryServiceImpl implements OrderHistoryService {
	
	Logger logger = LoggerFactory.getLogger(OrderHistoryServiceImpl.class);

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	OrderItemRepository orderItemRepository;
	
	/**
	 * 
	 * @param employeeId
	 * @return List<OrderItems>
	 */

	@Override
	public List<OrderItems> orderListByEmployee(long employeeId) throws EmployeeNotFoundException {
		
		logger.info("from the order history service");

		Employee employee = employeeRepository.findById(employeeId)
				.orElseThrow(() -> new EmployeeNotFoundException("Employee with given id not found"));

		List<OrderItems> order = orderItemRepository.findByEmployee(employee);

		List<OrderItems> orders = order.stream().sorted(Comparator.comparing(OrderItems::getId).reversed())
				.collect(Collectors.toList());
		int count = 0;
		List<OrderItems> items = new ArrayList<OrderItems>();
		for (OrderItems obj : orders) {
			items.add(obj);
			count++;
			if (count == 5) {

				break;
			}
		}

		return items;

	}

}
