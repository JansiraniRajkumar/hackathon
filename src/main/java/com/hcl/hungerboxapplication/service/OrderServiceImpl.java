package com.hcl.hungerboxapplication.service;

import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hcl.hungerboxapplication.dao.EmployeeRepository;
import com.hcl.hungerboxapplication.dao.ItemRepository;
import com.hcl.hungerboxapplication.dao.OrderItemRepository;
import com.hcl.hungerboxapplication.dao.OrderRepository;
import com.hcl.hungerboxapplication.dao.VendorRepository;
import com.hcl.hungerboxapplication.model.Employee;
import com.hcl.hungerboxapplication.model.Item;
import com.hcl.hungerboxapplication.model.Order;
import com.hcl.hungerboxapplication.model.OrderItems;
import com.hcl.hungerboxapplication.model.Vendor;
import com.hcl.hungerboxapplication.dto.OrderDto;
import com.hcl.hungerboxapplication.exception.EmployeeNotFoundException;
import com.hcl.hungerboxapplication.exception.PaymentNotSuccessfulException;
import com.hcl.hungerboxapplication.exception.VendorNotFoundException;

/**
 * 
 * @author Lakshmibhavani
 * This class is for making the orders
 *
 */
@Service
public class OrderServiceImpl {
	
	
	Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

	private static final long HUNGERBOXNUMBER = 9550608030L;

	@Autowired
	OrderItemRepository orderItemRepository;

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	VendorRepository vendorRepository;

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	ItemRepository itemRepository;

	@Autowired
	RestTemplate restTemplate;

	
	/**
	 * 
	 * @param orderDto
	 * @return Order
	 * @throws JSONException
	 * @throws PaymentNotSuccessfulException
	 * @throws EmployeeNotFoundException
	 * @throws VendorNotFoundException
	 */
	public Order makeOrder(OrderDto orderDto) throws JSONException, PaymentNotSuccessfulException ,EmployeeNotFoundException, VendorNotFoundException{
		Employee employee = employeeRepository.findById(orderDto.getEmployeeId())
				.orElseThrow(() -> new EmployeeNotFoundException("employee Not Found"));
		Vendor vendor = vendorRepository.findById(orderDto.getVendorId())
				.orElseThrow(() -> new VendorNotFoundException("vendor not found"));
    logger.info("this is for the employee can able to select multiple items");
		Order order = new Order();
		List<Item> items = orderDto.getItems();

		List<Double> d = items.stream().map(x -> x.getUnitPrice()).collect(Collectors.toList());
		double sum = d.stream().mapToDouble(Double::doubleValue).sum();
		order.setEmployee(employee);
		order.setOrderPrice(sum);
		order.setQuantity(items.size());
		orderRepository.save(order);
		for (Item item : items) {
			Item item1 = itemRepository.findByItemIdAndVendor(item.getItemId(), vendor);
			if (item1 != null) {

				OrderItems orderItem = new OrderItems();
				orderItem.setItem(item);
				orderItem.setOrder(order);
				orderItemRepository.save(orderItem);
			} else {
				throw new VendorNotFoundException("the item is not found related to particular vendor");
			}

		}

		try {
			String uri = "http://BANK-SERVICE/transaction";
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
            logger.info("this for make payement");
			JSONObject request = new JSONObject();
			request.put("fromMobileNumber", (long) employee.getPhone());
			request.put("toMobileNumber", HUNGERBOXNUMBER);
			request.put("transferAmmount", sum);
			HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

			ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

			System.out.println(response);
		} catch (Exception e) {
			logger.warn("you may get transaction failure exception");
			throw new PaymentNotSuccessfulException("transaction is failure");
		}
		return order;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
