package com.hcl.hungerboxapplication.dto;

import java.util.List;

import com.hcl.hungerboxapplication.model.Item;

public class OrderDto {

	private long employeeId;
	private long vendorId;
	private List<Item> items;
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public long getVendorId() {
		return vendorId;
	}
	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
}
