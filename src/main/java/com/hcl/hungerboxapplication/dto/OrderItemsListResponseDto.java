package com.hcl.hungerboxapplication.dto;

import java.util.List;

import com.hcl.hungerboxapplication.model.OrderItems;

public class OrderItemsListResponseDto {
	
	private Long orderId;
	
	private List<OrderItems> items;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public List<OrderItems> getItems() {
		return items;
	}

	public void setItems(List<OrderItems> items) {
		this.items = items;
	}
	
	

}
