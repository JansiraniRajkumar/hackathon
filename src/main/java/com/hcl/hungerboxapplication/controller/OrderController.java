package com.hcl.hungerboxapplication.controller;

import org.json.JSONException;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.hungerboxapplication.dto.OrderDto;
import com.hcl.hungerboxapplication.exception.EmployeeNotFoundException;
import com.hcl.hungerboxapplication.exception.PaymentNotSuccessfulException;
import com.hcl.hungerboxapplication.exception.VendorNotFoundException;
import com.hcl.hungerboxapplication.model.Order;
import com.hcl.hungerboxapplication.service.OrderServiceImpl;

/**
 * 
 * @author Lakshmibhavani version:1.0 this is class for Making the order by
 *         selecting multiple items
 *
 */
@RestController
@RequestMapping("/order")
public class OrderController {

	Logger logger = LoggerFactory.getLogger(OrderController.class);
	@Autowired
	OrderServiceImpl ordersService;

	/**
	 * 
	 * @param orderDto
	 * @return ResponseEntity<Order>
	 * @throws JSONException
	 * @throws PaymentNotSuccessfulException
	 * @throws EmployeeNotFoundException
	 * @throws VendorNotFoundException
	 */
	@PostMapping("")
	public ResponseEntity<Order> makingOrder(@Validated @RequestBody OrderDto orderDto)
			throws JSONException, PaymentNotSuccessfulException, EmployeeNotFoundException, VendorNotFoundException {
		logger.info("this is for selecting multiple items in controller");
		Order order = ordersService.makeOrder(orderDto);
		return new ResponseEntity<Order>(order, HttpStatus.OK);
	}

}
