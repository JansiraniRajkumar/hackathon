package com.hcl.hungerboxapplication.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.hungerboxapplication.exception.EmployeeNotFoundException;
import com.hcl.hungerboxapplication.model.OrderItems;
import com.hcl.hungerboxapplication.service.OrderHistoryService;

/**
 * 
 * @author Jansirani
 * @version 1.0 this is class for order history
 */
@RestController
@RequestMapping("/histories")
public class OrderHistoryController {

	Logger logger = LoggerFactory.getLogger(OrderHistoryController.class);

	@Autowired
	OrderHistoryService orderHistoryService;

	/**
	 * 
	 * @param employeeId
	 * @return List<OrderItems>
	 */

	@GetMapping("/history")
	public ResponseEntity<List<OrderItems>> getByEmployeeId(@RequestParam long employeeId)
			throws EmployeeNotFoundException {
		logger.info("from the order history controller");
		List<OrderItems> order = orderHistoryService.orderListByEmployee(employeeId);
		return new ResponseEntity<>(order, HttpStatus.OK);
	}
}
