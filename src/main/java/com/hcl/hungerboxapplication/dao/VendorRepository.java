package com.hcl.hungerboxapplication.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.hungerboxapplication.model.Vendor;

public interface VendorRepository extends JpaRepository<Vendor,Long>{

}
