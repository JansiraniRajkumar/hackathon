package com.hcl.hungerboxapplication.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.hungerboxapplication.model.Item;
import com.hcl.hungerboxapplication.model.Vendor;

public interface ItemRepository extends JpaRepository<Item, Long>{
	List<Item> findItemByNameLike(String string);
	List<Item> findItemByVendor(Vendor vendor1);
	Item findByItemIdAndVendor(long itemId,Vendor vendor);
}
