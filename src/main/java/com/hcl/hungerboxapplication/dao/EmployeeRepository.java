package com.hcl.hungerboxapplication.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.hungerboxapplication.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
