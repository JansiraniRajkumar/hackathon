package com.hcl.hungerboxapplication.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.hungerboxapplication.model.Employee;
import com.hcl.hungerboxapplication.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{

	List<Order> findOrderByEmployee(Employee employee);

}
